import { TextcomPage } from './app.po';

describe('textcom App', () => {
  let page: TextcomPage;

  beforeEach(() => {
    page = new TextcomPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
