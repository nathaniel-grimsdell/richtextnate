import {Component, OnInit} from '@angular/core';


@Component({
  selector: 'app-component',
  templateUrl: './app.component.html',
})

export class AppComponent implements OnInit {


  // https://www.npmjs.com/package/ng2-ck-editable

  content = `
<h1>
  <img alt="SUS Farms"
       src="/img/SUS-h1.png"
       style="float:left; padding:0px 5px; width:80%"/>
</h1>
  <p>
  This tracker can be used to monitor progress towards the achievement of legal
  requirements or better management practices. It serves as a checklist of each and every measure, against which one can
  indicate the degree of achievement of a requirement or practice.
</p>
    <h2>GETTING STARTED</h2>
      <ul>
  <li><p>Indicate the year for which this tracker is being completed. This reporting period must begin on 1 April of one
    year and end on the 31 March of the following year.</p></li>
  <li><p>Regardless of the date on which you complete the Tracker, the information entered must relate to this
    twelve-month reporting period. This will allow meaningful year-on-year progress tracking.</p></li>
  <li><p>Select different sections of the Tracker by clicking one of the tabs at the bottom of your screen.</p></li>
  <li><p>Start by selecting the PROFILE tab, and fill in the details requested.</p></li>
  <li><p>Next, proceed to the tree main sections of SUSFARMS, namely PROSPERITY, PEOPLE and PLANET.</p></li>
</ul>
    <h2>SCORING</h2>
      <ul>
  <li><p>Select an answer for each measure from the drop-down menu.</p></li>
  <li><p>Some measures allow the user to select a range of percentages from 0-100%. Choose a figure that best represents
    the degree to which you comply with the measure being rated.</p></li>
  <li><p>If a measure is not applicable to your situation, then select 'N/A'.</p></li>
  <li><p>If a measure requires you to enter a value, enter only the figure and not the unit of measure. For example,
    enter 65 and not 65 tc/ha.</p></li>
  <li><p> If you are unsure of how to score a particular measure, you may wish to consult the SUSFARMS Manual. </p></li>
  <li><p>All necessary calculations are done by the Tracker in the background. To gain an understanding of the criteria
    and the logic used in the calculations, consult the SUSFARMS Manual.</p></li>
</ul>
    <h2>SUMMARY REPORT</h2>
      <ul>
  <li>
    <p>
      Once completed, the pages Tracker provides a REPORT. This report can be used to develop an action plan that can
      serve to plan the way forward towards the implementation and achievement of new measures.
    </p>
  </li>
</ul>
`;


  editor: any;

  constructor() {
  }

  ngOnInit() {
  }

}
