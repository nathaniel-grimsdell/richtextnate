import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule} from '@angular/forms';
// import { Ng2CKEditableModule } from 'ng2-ck-editable';
import { CKEditorModule } from 'ng2-ckeditor';

import {AppComponent} from './app.component';



@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    CKEditorModule,
    // Ng2CKEditableModule.forRoot()
  ],
  declarations: [
    AppComponent
  ],
  bootstrap: [
    AppComponent
  ],
  providers: []
})

export class AppModule {


}

//
// import { NgModule } from '@angular/core';
//
// import { QuillEditorComponent } from './app.component';
//
// @NgModule({
//   declarations: [
//     QuillEditorComponent
//   ],
//   exports: [
//     QuillEditorComponent
//   ]
// })
// export class QuillEditorModule {}
